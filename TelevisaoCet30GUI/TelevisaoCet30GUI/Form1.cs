﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TelevisaoCet30GUI
{
    public partial class Form1 : Form
        
    {
        private Tv minhaTv;



        public Form1()
        {
            InitializeComponent();

            minhaTv = new Tv();

            LabelStatus.Text = minhaTv.EnviaMensagem();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void ButtonOnOff_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void LabelCanal_Click(object sender, EventArgs e)
        {

        }

        private void ButtonAumentaCanal_Click(object sender, EventArgs e)
        {
            minhaTv.MudaCanal(minhaTv.GetCanal() + 1);

            LabelCanal.Text = minhaTv.GetCanal().ToString();



        }

        private void ButtonDiminuiCanal_Click(object sender, EventArgs e)
        {
            minhaTv.MudaCanal(minhaTv.GetCanal() - 1);

            LabelCanal.Text = minhaTv.GetCanal().ToString();
        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void TrackBarVolume_Scroll(object sender, EventArgs e)
        {

            minhaTv.SetVolume(TrackBarVolume.Value);

            LabelVolume.Text = minhaTv.GetVolume().ToString();

            

            
        }
    }
}
