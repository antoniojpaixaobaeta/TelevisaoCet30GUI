﻿namespace TelevisaoCet30GUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelStatus = new System.Windows.Forms.Label();
            this.ButtonOnOff = new System.Windows.Forms.Button();
            this.Canais = new System.Windows.Forms.GroupBox();
            this.LabelCanal = new System.Windows.Forms.Label();
            this.ButtonAumentaCanal = new System.Windows.Forms.Button();
            this.ButtonDiminuiCanal = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TrackBarVolume = new System.Windows.Forms.TrackBar();
            this.LabelVolume = new System.Windows.Forms.Label();
            this.Canais.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarVolume)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelStatus
            // 
            this.LabelStatus.AutoSize = true;
            this.LabelStatus.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelStatus.Location = new System.Drawing.Point(12, 159);
            this.LabelStatus.Name = "LabelStatus";
            this.LabelStatus.Size = new System.Drawing.Size(46, 19);
            this.LabelStatus.TabIndex = 0;
            this.LabelStatus.Text = "Cenas";
            this.LabelStatus.Click += new System.EventHandler(this.label1_Click);
            // 
            // ButtonOnOff
            // 
            this.ButtonOnOff.Location = new System.Drawing.Point(221, 199);
            this.ButtonOnOff.Name = "ButtonOnOff";
            this.ButtonOnOff.Size = new System.Drawing.Size(51, 50);
            this.ButtonOnOff.TabIndex = 2;
            this.ButtonOnOff.Text = "ON";
            this.ButtonOnOff.UseVisualStyleBackColor = true;
            this.ButtonOnOff.Click += new System.EventHandler(this.ButtonDiminuiCanal_Click);
            // 
            // Canais
            // 
            this.Canais.Controls.Add(this.LabelCanal);
            this.Canais.Controls.Add(this.ButtonAumentaCanal);
            this.Canais.Controls.Add(this.ButtonDiminuiCanal);
            this.Canais.Location = new System.Drawing.Point(135, 51);
            this.Canais.Name = "Canais";
            this.Canais.Size = new System.Drawing.Size(137, 110);
            this.Canais.TabIndex = 3;
            this.Canais.TabStop = false;
            this.Canais.Text = "Canais";
            this.Canais.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // LabelCanal
            // 
            this.LabelCanal.AutoSize = true;
            this.LabelCanal.Location = new System.Drawing.Point(41, 72);
            this.LabelCanal.Name = "LabelCanal";
            this.LabelCanal.Size = new System.Drawing.Size(0, 13);
            this.LabelCanal.TabIndex = 7;
            this.LabelCanal.Click += new System.EventHandler(this.LabelCanal_Click);
            // 
            // ButtonAumentaCanal
            // 
            this.ButtonAumentaCanal.Enabled = false;
            this.ButtonAumentaCanal.Location = new System.Drawing.Point(86, 29);
            this.ButtonAumentaCanal.Name = "ButtonAumentaCanal";
            this.ButtonAumentaCanal.Size = new System.Drawing.Size(27, 27);
            this.ButtonAumentaCanal.TabIndex = 6;
            this.ButtonAumentaCanal.Text = "+";
            this.ButtonAumentaCanal.UseVisualStyleBackColor = true;
            this.ButtonAumentaCanal.Click += new System.EventHandler(this.ButtonAumentaCanal_Click);
            // 
            // ButtonDiminuiCanal
            // 
            this.ButtonDiminuiCanal.Enabled = false;
            this.ButtonDiminuiCanal.Location = new System.Drawing.Point(20, 29);
            this.ButtonDiminuiCanal.Name = "ButtonDiminuiCanal";
            this.ButtonDiminuiCanal.Size = new System.Drawing.Size(28, 27);
            this.ButtonDiminuiCanal.TabIndex = 4;
            this.ButtonDiminuiCanal.Text = "-";
            this.ButtonDiminuiCanal.UseVisualStyleBackColor = true;
            this.ButtonDiminuiCanal.Click += new System.EventHandler(this.ButtonDiminuiCanal_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.LabelVolume);
            this.groupBox1.Controls.Add(this.TrackBarVolume);
            this.groupBox1.Location = new System.Drawing.Point(44, 276);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(169, 116);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // TrackBarVolume
            // 
            this.TrackBarVolume.Location = new System.Drawing.Point(35, 42);
            this.TrackBarVolume.Maximum = 100;
            this.TrackBarVolume.Name = "TrackBarVolume";
            this.TrackBarVolume.Size = new System.Drawing.Size(104, 45);
            this.TrackBarVolume.TabIndex = 0;
            this.TrackBarVolume.Scroll += new System.EventHandler(this.TrackBarVolume_Scroll);
            // 
            // LabelVolume
            // 
            this.LabelVolume.AutoSize = true;
            this.LabelVolume.Location = new System.Drawing.Point(78, 74);
            this.LabelVolume.Name = "LabelVolume";
            this.LabelVolume.Size = new System.Drawing.Size(13, 13);
            this.LabelVolume.TabIndex = 5;
            this.LabelVolume.Text = "--";
            this.LabelVolume.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 474);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ButtonOnOff);
            this.Controls.Add(this.LabelStatus);
            this.Controls.Add(this.Canais);
            this.Name = "Form1";
            this.Text = "Televisao";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Canais.ResumeLayout(false);
            this.Canais.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarVolume)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelStatus;
        private System.Windows.Forms.Button ButtonOnOff;
        private System.Windows.Forms.GroupBox Canais;
        private System.Windows.Forms.Button ButtonDiminuiCanal;
        private System.Windows.Forms.Label LabelCanal;
        private System.Windows.Forms.Button ButtonAumentaCanal;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TrackBar TrackBarVolume;
        private System.Windows.Forms.Label LabelVolume;
    }
}

